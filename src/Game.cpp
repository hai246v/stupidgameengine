// Copyright<2019><Hai Vu>
//     Permission is hereby granted, free of charge,
//     to any person obtaining a copy of this software
//     and associated documentation files(the "Software"),
//     to deal in the Software without restriction, including
//     without limitation the rights to use, copy, modify,
//     merge, publish, distribute, sublicense, and / or sell
//     copies of the Software, and to permit persons to whom
//     the Software is furnished to do so, subject to the
//     following conditions : The above copyright notice and t
//     his permission notice shall be included in all copies
//     or substantial portions of the Software.THE SOFTWARE
//     IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//     EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//     WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//     PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE AUTHORS
//     OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
//     OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//     OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//     SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

//===================================================================
/**
* FILE: Game.cpp
* TODO: 
*/
//===================================================================

#include "Main.hpp"

using namespace he;

//==========CLASS IMPLEMENTATION=====================================
/**
 * CLASS: Game 
 * TODO: 
*/
//===================================================================

Game::Game()
{
    std ::cout << "Created Game instance." << std::endl;
}

Game::~Game()
{
    std::cout << "Destroyed Game instance." << std::endl;
}

Game *Game::m_Instance = NULL;

Game *Game::getInstance()
{
    if (m_Instance == NULL)
        m_Instance = new Game();
    return m_Instance;
}

void Game::destroyInstance()
{
    std::cout << "Destroying Game instance..." << std::endl;
    delete m_Instance;
}

void Game::initialize()
{
    std::cout << "Initializing application..." << std::endl;
    initializeGraphic();
    initializeAudio();
    std::cout << "Finished initializing application!" << std::endl;
}

void Game::runLoop()
{
    m_GraphicDevice->update();
    glClearColor(1.0, 0, 0, 1);
    SDL_Delay(2000); // testing purpose
    m_GraphicDevice->update();
    SDL_Delay(2000);
}

void Game::initializeGraphic()
{
    m_GraphicDevice = new GraphicService();
    m_GraphicDevice->initialize((Game *)this);
}

void Game::initializeAudio()
{
    m_AudioDevice = new AudioService();
    m_AudioDevice->initialize((Game *)this);
}

void Game::cleanUp()
{
    std::cout << "Cleaning up application..." << std::endl;
    m_AudioDevice->cleanUp();
    m_GraphicDevice->cleanUp();
    delete m_AudioDevice;
    delete m_GraphicDevice;
    std::cout << "Finished cleaning up application!" << std::endl;
}

AudioService *Game::getAudioService()
{
    return m_AudioDevice;
}

GraphicService *Game::getGraphicService()
{
    return m_GraphicDevice;
}