// Copyright<2019><Hai Vu>
//     Permission is hereby granted, free of charge,
//     to any person obtaining a copy of this software
//     and associated documentation files(the "Software"),
//     to deal in the Software without restriction, including
//     without limitation the rights to use, copy, modify,
//     merge, publish, distribute, sublicense, and / or sell
//     copies of the Software, and to permit persons to whom
//     the Software is furnished to do so, subject to the
//     following conditions : The above copyright notice and t
//     his permission notice shall be included in all copies
//     or substantial portions of the Software.THE SOFTWARE
//     IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//     EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//     WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//     PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE AUTHORS
//     OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
//     OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//     OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//     SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

//===================================================================
/**
* FILE: Graphics.hpp
* TODO: 
*/
//===================================================================

#ifndef _GRAPHICS_HPP_
#define _GRAPHICS_HPP_

#include "Main.hpp"

namespace he
{

class GraphicService
{
public:
    GraphicService();
    ~GraphicService();
    void initialize(Game *game);
    void cleanUp();
    void update();

private:
    void initializeRenderDevice();
    void initializeDisplayDevice();
    Game *m_Game;
    RenderDevice *m_RenderDevice;
    DisplayDevice *m_DisplayDevice;
};

class DisplayDevice
{
public:
    void initialize();
    void update();

private:
    SDL_Window *m_SDL_Window;
};

class RenderDevice
{
public:
    void initialize();
    void update();

private:
    GLuint m_ProgramId;
};

} // namespace he

#endif