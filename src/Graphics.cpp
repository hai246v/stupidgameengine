// Copyright<2019><Hai Vu>
//     Permission is hereby granted, free of charge,
//     to any person obtaining a copy of this software
//     and associated documentation files(the "Software"),
//     to deal in the Software without restriction, including
//     without limitation the rights to use, copy, modify,
//     merge, publish, distribute, sublicense, and / or sell
//     copies of the Software, and to permit persons to whom
//     the Software is furnished to do so, subject to the
//     following conditions : The above copyright notice and t
//     his permission notice shall be included in all copies
//     or substantial portions of the Software.THE SOFTWARE
//     IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//     EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//     WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//     PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE AUTHORS
//     OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
//     OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//     OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//     SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

//===================================================================
/**
* FILE: Graphics.cpp
* TODO: 
*/
//===================================================================

#include "Main.hpp"

using namespace he;

//==========CLASS IMPLEMENTATION=====================================
/**
 * CLASS: GraphicService 
 * TODO: 
*/
//===================================================================

GraphicService::GraphicService()
{
    std::cout << "Created GraphicService instance." << std::endl;
}

GraphicService::~GraphicService()
{
    std::cout << "Destroyed GraphicService instance." << std::endl;
}

void GraphicService::initialize(Game *game)
{
    std::cout << "Initializing graphic service..." << std::endl;
    initializeDisplayDevice();
    initializeRenderDevice();
    m_Game = game;
}

void GraphicService::cleanUp()
{
    std::cout << "Cleaning up graphic service..." << std::endl;
    delete m_RenderDevice;
    delete m_DisplayDevice;
}

void GraphicService::update()
{
    m_RenderDevice->update();
    m_DisplayDevice->update();
}

void GraphicService::initializeRenderDevice()
{
    m_RenderDevice = new RenderDevice();
    m_RenderDevice->initialize();
}

void GraphicService::initializeDisplayDevice()
{
    m_DisplayDevice = new DisplayDevice();
    m_DisplayDevice->initialize();
}

//==========CLASS IMPLEMENTATION=====================================
/**
 * CLASS: DisplayDevice 
 * TODO: 
*/
//===================================================================

#define SET_GL_VERSION(major, minor)                          \
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, major); \
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, minor);

#define CREATE_WINDOW(W, H)                                                                    \
    m_SDL_Window = SDL_CreateWindow("Game", 0, 0, W, H, SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL); \
    CRASH_IF(m_SDL_Window == NULL, "init window failed"); // Check if the window fail

void DisplayDevice::initialize()
{
    CRASH_IF(SDL_Init(SDL_INIT_VIDEO) < 0, "init video failed"); // Initialize SDL
    SET_GL_VERSION(2, 1);
    CREATE_WINDOW(1280, 720);
    CRASH_IF(SDL_GL_CreateContext(m_SDL_Window) == NULL, "create context failed"); // Initialize OpenGL context
}

void DisplayDevice::update()
{
    SDL_GL_SwapWindow(m_SDL_Window);
}

#undef SET_GL_VERSION
#undef CREATE_WINDOW

//==========CLASS IMPLEMENTATION=====================================
/**
 * CLASS: RenderDevice 
 * TODO: 
*/
//===================================================================

#define ENABLE_VSYNC() SDL_GL_SetSwapInterval(1);
#define SET_CLEAR_COLOR(r, g, b, a) glClearColor(r, g, b, a);
#define CLEAR_COLOR() glClear(GL_COLOR_BUFFER_BIT);

void RenderDevice::initialize()
{
    SET_CLEAR_COLOR(1, 0, 1, 1); // Set clear color to purple =))
    ENABLE_VSYNC();
}

void RenderDevice::update()
{
    CLEAR_COLOR(); // Clear the fucking screen
}

#undef CLEAR_COLOR
#undef ENABLE_VSYNC
#undef SET_CLEAR_COLOR